//
//  ConversationCell.swift
//  ChatApp
//
//  Created by Orlando G. Rodriguez on 11/20/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import UIKit

class ConversationCell: UITableViewCell {
    
    // MARK: - Model
    var conversation: Conversation?
    
    static let reuseIdentifier = "ConversationCell"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    func updateViewFromModel() {
        self.textLabel?.text = conversation?.participants[1]
        self.detailTextLabel?.text = conversation?.messages.last?.text
    }
    
    func updateModel(conversation: Conversation) {
        self.conversation = conversation
        updateViewFromModel()
    }

}
