//
//  MessageCell.swift
//  ChatApp
//
//  Created by Orlando G. Rodriguez on 11/16/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {
    
    // MARK: - Model
    var message: Message?
    var isMessageSentByCurrentUser: Bool?
    
    static let reuseIdentifier = "MessageCell"
    
    var oMessageLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textColor = .white
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var oMessageBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 16
        return view
    }()
    
    
    func setupUI() {
        addMessageLabel()
        addMessageBackground()
    }
    
    func updateModel(message: Message) {
        self.message = message
        updateViewFromModel()
    }
    
    func setMessageSentByCurrentUser(sentByUser: Bool) {
        isMessageSentByCurrentUser = sentByUser
    }
    
    func updateViewFromModel() {
        guard let msg = message else {
            print("Model was not updated. Please fix this bug.")
            return
        }
        oMessageLabel.text = msg.text
        addMessageLabelConstraints()
        addMessageBackgroundConstraints()
    }
    
    func addMessageLabel() {
        addSubview(oMessageLabel)
    }
    
    func addMessageBackground() {
        addSubview(oMessageBackgroundView)
        sendSubviewToBack(oMessageBackgroundView)
    }
    
    func addMessageBackgroundConstraints() {
        guard let msg = message else {
            print("")
            return
        }
        
        oMessageBackgroundView.topAnchor.constraint(equalTo: oMessageLabel.topAnchor, constant: -8).isActive = true
        oMessageBackgroundView.rightAnchor.constraint(equalTo: oMessageLabel.rightAnchor, constant: 16).isActive = true
        oMessageBackgroundView.bottomAnchor.constraint(equalTo: oMessageLabel.bottomAnchor, constant: 8).isActive = true
        oMessageBackgroundView.leftAnchor.constraint(equalTo: oMessageLabel.leftAnchor, constant: -16).isActive = true
        let sentByUser = ConversationsManager.currentUser == msg.sender
        if sentByUser {
            oMessageBackgroundView.backgroundColor = .red
            oMessageLabel.textColor = .white
        } else {
            oMessageBackgroundView.backgroundColor = .blue
            oMessageLabel.textColor = .white
        }
    }
    
    func addMessageLabelConstraints() {
        
        guard let msg = message else {
            print("")
            return
        }
        
        let sentByUser = ConversationsManager.currentUser == msg.sender
        
        oMessageLabel.widthAnchor.constraint(lessThanOrEqualTo: self.widthAnchor, constant: -128).isActive = true
        oMessageLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        self.heightAnchor.constraint(equalTo: oMessageLabel.heightAnchor, constant: 32).isActive = true
        
        if sentByUser {
            oMessageLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -44).isActive = true
            oMessageLabel.textAlignment = .right
        } else {
            oMessageLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 44).isActive = true
            oMessageLabel.textAlignment = .left
        }
        
    }
}
