//
//  MessageCell.swift
//  ChatApp
//
//  Created by Orlando G. Rodriguez on 11/16/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import UIKit
import FirebaseAuth

class MessageCell: UITableViewCell {
    
    // MARK: - Model
    var message: Message? {
        didSet {
            let currentUserID = FirebaseAPIService.currentLocalUser!.uid
            if currentUserID == message?.fromID {
                isMessageSentByCurrentUser = true
            } else {
                isMessageSentByCurrentUser = false
            }
        }
    }
    var isMessageSentByCurrentUser: Bool? {
        didSet {
            updateViewFromModel()
        }
    }
    
    static let reuseIdentifier = "MessageCell"
    
    var oMessageLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textColor = .white
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        label.setContentHuggingPriority(.fittingSizeLevel, for: .horizontal)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var oMessageBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 16
        return view
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    override func prepareForReuse() {
        for constraint in self.oMessageLabel.constraints {
            self.oMessageLabel.removeConstraint(constraint)
        }
        for constraint in self.constraints {
            self.removeConstraint(constraint)
        }
        for constraint in self.oMessageBackgroundView.constraints {
            self.oMessageBackgroundView.removeConstraint(constraint)
        }
        for view in subviews {
            view.removeFromSuperview()
        }
    }
    
    
    func setupUI() {
        addMessageLabel()
        addMessageBackground()
    }
    
    func updateModel(message: Message) {
        self.message = message
    }
    
    func updateViewFromModel() {
        guard let msg = message else {
            print("Model was not updated. Please fix this bug.")
            return
        }
        oMessageLabel.text = msg.text
        addConstraints()
    }
    
    func addMessageLabel() {
        addSubview(oMessageLabel)
    }
    
    func addMessageBackground() {
        addSubview(oMessageBackgroundView)
        sendSubviewToBack(oMessageBackgroundView)
    }
    
    func addConstraints() {
        
        self.oMessageBackgroundView.topAnchor.constraint(equalTo: self.oMessageLabel.topAnchor, constant: -8).isActive = true
        self.oMessageBackgroundView.rightAnchor.constraint(equalTo: self.oMessageLabel.rightAnchor, constant: 16).isActive = true
        self.oMessageBackgroundView.bottomAnchor.constraint(equalTo: self.oMessageLabel.bottomAnchor, constant: 8).isActive = true
        self.oMessageBackgroundView.leftAnchor.constraint(equalTo: self.oMessageLabel.leftAnchor, constant: -16).isActive = true
        
        
        if self.isMessageSentByCurrentUser! {
            self.oMessageLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -44).isActive = true
            self.oMessageLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 44).isActive = false
            self.oMessageBackgroundView.backgroundColor = Colors.purple
            self.oMessageLabel.textColor = .white
        } else {
            self.oMessageLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -44).isActive = false
            self.oMessageLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 44).isActive = true
            self.oMessageBackgroundView.backgroundColor = Colors.lightGray
            self.oMessageLabel.textColor = .black
        }
        self.oMessageLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        self.heightAnchor.constraint(equalTo: self.oMessageLabel.heightAnchor, constant: 32).isActive = true
        self.oMessageLabel.widthAnchor.constraint(lessThanOrEqualTo: self.widthAnchor, constant: -128).isActive = true
        
    }
}
