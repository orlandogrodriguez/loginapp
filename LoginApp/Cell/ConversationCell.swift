//
//  ConversationCell.swift
//  ChatApp
//
//  Created by Orlando G. Rodriguez on 11/20/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import UIKit

class ConversationCell: UITableViewCell {
    
    // MARK: - Model
    var conversation: Message? {
        didSet {
            
            var idToCheck: String
            if conversation?.toID == FirebaseAPIService.currentLocalUser?.uid {
                idToCheck = conversation!.fromID
            } else {
                idToCheck = conversation!.toID
            }
            
            FirebaseAPIService.getEmailFromUID(uid: idToCheck) { (email) in
                self.receiver = email
                self.messageText = (self.conversation?.text)!
                self.updateViewFromModel()
            }
        }
    }
    var receiver: String?
    var messageText: String?
    
    static let reuseIdentifier = "ConversationCell"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    func updateViewFromModel() {
        self.textLabel?.text = receiver
        self.detailTextLabel?.text = messageText
    }
    
    func updateModel(conversation: Message) {
        self.conversation = conversation
    }
}
