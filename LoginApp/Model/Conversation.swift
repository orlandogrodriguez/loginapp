//
//  Conversation.swift
//  ChatApp
//
//  Created by Orlando G. Rodriguez on 11/16/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct Conversation {
    var receiver: LocalUser
    var text: String
    init(receiver: LocalUser, text: String) {
        self.receiver = receiver
        self.text = text
    }
}
