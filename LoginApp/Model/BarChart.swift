//
//  BarChart.swift
//  LoginApp
//
//  Created by Orlando G. Rodriguez on 11/28/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import Foundation
import UIKit

struct BarChart {
    
    var title: String
    var values: [String:Double]
    var max: Double {
        get {
            var curMax: Double = 0
            for value in values {
                if value.value > curMax {
                    curMax = value.value
                }
            }
            return curMax
        }
    }
    
    init(title: String, values: [String:Double]) {
        self.title = title
        self.values = values
    }
}
