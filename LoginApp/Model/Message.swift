//
//  Message.swift
//  ChatApp
//
//  Created by Orlando G. Rodriguez on 11/16/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import Foundation

struct Message {
    
    var toID: String
    var text: String
    var date: Date
    var fromID: String
    
    init(text: String, toID: String, fromID: String) {
        self.text = text
        self.toID = toID
        self.fromID = fromID
        self.date = Date(timeIntervalSinceNow: 0)
    }
    
    init(text: String, toID: String, fromID: String, date: TimeInterval) {
        self.text = text
        self.toID = toID
        self.fromID = fromID
        self.date = Date(timeIntervalSince1970: date)
    }
    
}
