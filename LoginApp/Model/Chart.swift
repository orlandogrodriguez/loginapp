//
//  File.swift
//  LoginApp
//
//  Created by Orlando G. Rodriguez on 11/28/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import Foundation

protocol Chart {
    func draw()
}
