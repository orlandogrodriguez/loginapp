//
//  User.swift
//  ChatApp
//
//  Created by Orlando G. Rodriguez on 11/21/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import Foundation
class LocalUser {
    var name: String?
    var email: String
    var uid: String
    init(uid: String, email: String) {
        self.uid = uid
        self.email = email
    }
    
    init(uid: String) {
        self.uid = uid
        self.email = ""
        FirebaseAPIService.getEmailFromUID(uid: uid) { (email) in
            self.email = email
        }
    }
}
