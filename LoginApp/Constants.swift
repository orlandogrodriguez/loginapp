//
//  Constants.swift
//  LoginApp
//
//  Created by Orlando G. Rodriguez on 11/11/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import UIKit

struct Colors {
    
    // Gradients
    static let intro = Gradient(color1: UIColor(rgb: 0x4F0C42), color2: UIColor(rgb: 0x9C0B4E))
    
    // Colors
    static let red = UIColor(rgb: 0xEA2A56)
    static let purple = UIColor(rgb: 0xE74279)
    static let lightGray = UIColor(rgb: 0xECF0F1)
}

struct Gradient {
    
    var color1: UIColor
    var color2: UIColor
    var color3: UIColor?
    
    init(color1: UIColor, color2: UIColor) {
        self.color1 = color1
        self.color2 = color2
    }
    
    init(color1: UIColor, color2: UIColor, color3: UIColor) {
        self.init(color1: color1, color2: color2)
        self.color3 = color3
    }
    
    func gradient() -> [CGColor] {
        if let color3 = color3 {
            return [color1.cgColor, color2.cgColor, color3.cgColor]
        } else {
            return [color1.cgColor, color2.cgColor]
        }
    }
}
