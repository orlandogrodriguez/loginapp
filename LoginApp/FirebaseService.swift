//
//  FirebaseService.swift
//  LoginApp
//
//  Created by Orlando G. Rodriguez on 11/29/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth

class FirebaseService {
    
    static var conversations: [String:Message] = [:]
    static var messages: [String:Message] = [:]
    static var uidToEmailDictionary: [String:String] = [:]
    static var users: [String:LocalUser] = [:]
    static var currentLocalUser: LocalUser?
    static var ref = Database.database().reference()
    
    static func observeUserChanges(completion: @escaping () -> ()) {
        FirebaseService.ref.child("users").observe(.value) { (snapshot) in
            let usersDict = snapshot.value as! NSDictionary
            for userKV in usersDict {
                let userValue = userKV.value as! NSDictionary
                guard let userEmail = userValue.value(forKey: "email") as? String else { return }
                print()
                guard let userID = userKV.key as? String else { return }
                uidToEmailDictionary[userID] = userEmail
                users[userID] = LocalUser(uid: userID, email: userEmail)
                if FirebaseService.uidToEmailDictionary.count == usersDict.count {
                    completion()
                }
            }
        }
    }
    
    static func observeMessageChanges() {
        FirebaseService.currentLocalUser = LocalUser(uid: (Auth.auth().currentUser?.uid)!, email: (Auth.auth().currentUser?.email)!)
        FirebaseService.ref.child("messages").observe(.value) { (snapshot) in
            guard let messagesDict = snapshot.value as? NSDictionary else {
                print("No messages available yet.")
                return
            }
            for messageIDKV in messagesDict {
                let messageValues = messageIDKV.value as! NSDictionary
                let msg = Message(text: messageValues.value(forKey: "text") as! String,
                                  toID: messageValues.value(forKey: "toID") as! String,
                                  fromID: messageValues.value(forKey: "fromID") as! String,
                                  date: messageValues.value(forKey: "date") as! TimeInterval)
                if FirebaseService.messages[messageIDKV.key as! String] != nil {
                    print("Message of id \(messageIDKV.key as! String) already exists.")
                    print("Not adding it to messages.")
                } else {
                    FirebaseService.messages[messageIDKV.key as! String] = msg
                }
                
                var conversationEmail: String = ""
                if msg.fromID == FirebaseService.currentLocalUser?.uid {
                    conversationEmail = FirebaseService.uidToEmailDictionary[msg.toID]!
                } else if msg.toID == FirebaseService.currentLocalUser?.uid{
                    conversationEmail = FirebaseService.uidToEmailDictionary[msg.fromID]!
                } else {
                    print("This message does not involve the current user.")
                    continue
                }
                

                if FirebaseService.conversations[conversationEmail] != nil {
                    if FirebaseService.conversations[conversationEmail]!.date < msg.date {
                        FirebaseService.conversations[conversationEmail] = msg
                    }
                } else {
                    FirebaseService.conversations[conversationEmail] = msg
                }
            }
            print("loadedAllMessages")
            NotificationCenter.default.post(Notification(name: Notification.Name("loadedAllMessages")))
        }
    }
    
    static func createNewMessage(message: Message) {
        let newMessageRef = FirebaseAPIService.ref.child("messages").childByAutoId()
        newMessageRef.updateChildValues(["toID" : message.toID,
                                         "fromID" : message.fromID,
                                         "text" : message.text,
                                         "date" : Int(message.date.timeIntervalSince1970)])
        newMessageRef.observeSingleEvent(of: .value) { (snapshot) in
            let values = snapshot.value as! [String: Any?]
            let text = values["text"] as! String
            let toID = values["toID"] as! String
            let fromID = values["fromID"] as! String
            let timeInterval = values["date"] as! TimeInterval
            let newMessage = Message(text: text, toID: toID, fromID: fromID, date: timeInterval)
            ConversationManager.currentChatLog.append(newMessage)
        }
    }
}
