//
//  FirebaseAPIService.swift
//  LoginApp
//
//  Created by Orlando G. Rodriguez on 11/14/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

class FirebaseAPIService {
    static let ref = Database.database().reference()
    static var currentUser: User? {
        get {
            guard let user = Auth.auth().currentUser else { return nil }
            return user
        }
    }
    
    static var currentLocalUser: LocalUser? {
        get {
            guard let user = self.currentUser,
                let email = user.email else {
                return nil
            }
            let localUser = LocalUser(uid: user.uid, email: email)
            return localUser
        }
    }
    
    static var allUserIDs: [String]? {
        didSet {
            print("users updated: ")
            //print(allUserIDs!)
            self.getEmails()
        }
    }
    
    static var allEmails: [String]? {
        didSet {
            print("emails updated")
            // print(allEmails!)
        }
    }
    
    static var allUsers: [LocalUser] = []
    
    
    init() {
        print("Creating firebase api service.")
        Auth.auth().addStateDidChangeListener { (auth, user) in
            if user != nil {
                print("Posting notification: userLoggedIn")
                NotificationCenter.default.post(name: Notification.Name(rawValue: "userLoggedIn"), object: nil)
                self.addUserToDatabase(user: user!)
            } else {
                print("Posting notification: userLoggedOut")
                NotificationCenter.default.post(name: Notification.Name(rawValue: "userLoggedOut"), object: nil)
            }
        }
    }
    
    func login(with email: String, _ password: String) {
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if let error = error {
                print("Error signing in...")
                print(error.localizedDescription)
                return
            }
            guard let user = user else { return }
            print("Successfully signed in user: \(String(describing: user.user.email))")
            NotificationCenter.default.post(name: Notification.Name(rawValue: "animateFadeOutAllLoginViewElements"), object: nil)
            
        }
    }
    
    func signup(with email: String, password: String, repeatPassword: String) {
        if (password != repeatPassword) {
            print("Pasword confirmation failed. Make sure both passwords are the same.")
            return
        }
        Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
            if let error = error {
                print("Error signing in...")
                print(error.localizedDescription)
                return
            }
            guard let user = authResult?.user else { return }
            print("Successfully created new user: \(user.email!)")
            self.addUserToDatabase(user: user)
            NotificationCenter.default.post(name: Notification.Name(rawValue: "animateFadeOutRegisterViewElements"), object: nil)
        }
    }
    
    func addUserToDatabase(user: User) {
        print("Adding user to database...")
        let ref = Database.database().reference()
        ref.child("users").child(user.uid).setValue(["email":user.email])
    }
    
    func addMessageToDatabase(message: Message) {
        let ref = Database.database().reference()
        ref.child("messages").childByAutoId().setValuesForKeys([
            "toID":message.toID,
            "fromID":message.fromID,
            "text":message.text,
            "date":Int(message.date.timeIntervalSince1970)])
    }
    
    func userDidConfirmEmail(user: User) -> Bool {
        print("Checking email confirmation...")
        if user.isEmailVerified {
            return true
        }
        return false
    }
    
    func resendConfirmationEmail() {
        guard let user = Auth.auth().currentUser else {
            print("No user is currently signed in...")
            return
        }
        user.sendEmailVerification { (error) in
            if let error = error {
                print("Unable to send email verification.")
                NotificationCenter.default.post(name: Notification.Name(rawValue: "verificationEmailSentUnsuccessfully"), object: nil)
                print(error.localizedDescription)
            }
            guard let userEmail = user.email else { return }
            print("Successfully sent email verification to \(userEmail)")
            NotificationCenter.default.post(name: Notification.Name(rawValue: "verificationEmailSentSuccessfully"), object: nil)
        }
    }
    
    func logoutCurrentUser() {
        do {
            try Auth.auth().signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    static func getContacts() {
        // Get all users.
        let ref = Database.database().reference().child("users")
        ref.observeSingleEvent(of: DataEventType.value) { (snapshot) in
            let dictionary = snapshot.value as! NSDictionary
            let contactIDs = dictionary.allKeys as! [String]
            self.allUserIDs = contactIDs
        }
    }
    
    static func getEmails() {
        // Get all emails
        let ref = Database.database().reference().child("users")
        guard let users = self.allUserIDs else {
            print("Could not find all users.")
            return
        }
        self.allUsers = []
        for uid in users {
            ref.child(uid).observeSingleEvent(of: DataEventType.value) { (snapshot) in
                let emailDictionary = snapshot.value as! NSDictionary
//                print("emails: \(emailDictionary)")
                let email = emailDictionary.allValues
                let user = LocalUser(uid: uid, email: email[0] as! String)
                self.allEmails?.append(email[0] as! String)
                self.allUsers.append(user)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "usersUpdated"), object: nil)
            }
        }
    }

    static func fetchEmailFrom(uid: String, completion: @escaping ([String]?) -> ()){
        let userRef = ref.child("users").child(uid)
        userRef.observeSingleEvent(of: .value) { (snapshot) in
            guard let data = snapshot.value as? NSDictionary else {
                print("Could not load email address for \(uid)")
                return
            }
            let userEmails: [String] = data.allKeys as! [String]
            completion(userEmails)
        }
    }
    
    static func createNewMessage(message: Message) {
        let newMessageRef = FirebaseAPIService.ref.child("messages").childByAutoId()
        newMessageRef.updateChildValues(["toID" : message.toID,
                                         "fromID" : message.fromID,
                                         "text" : message.text,
                                         "date" : Int(message.date.timeIntervalSince1970)])
        newMessageRef.observeSingleEvent(of: .value) { (snapshot) in
            let values = snapshot.value as! [String: Any?]
            let text = values["text"] as! String
            let toID = values["toID"] as! String
            let fromID = values["fromID"] as! String
            let timeInterval = values["date"] as! TimeInterval
            let newMessage = Message(text: text, toID: toID, fromID: fromID, date: timeInterval)
            ConversationManager.currentChatLog.append(newMessage)
        }
    }
    
    static func loadChatlog(for participant0: LocalUser, and participant1: LocalUser) {
        let messagesRef = ref.child("messages")
        messagesRef.observeSingleEvent(of: .value) { (snapshot) in
            let values = snapshot.value as! NSDictionary
            ConversationManager.allMessageIDs = values.allKeys as? [String]
            guard let allIDs = ConversationManager.allMessageIDs else {
                print("Message IDs were not updated.")
                return
            }
            var messages: [Message] = []
            for uid in allIDs {
                FirebaseAPIService.ref.child("messages").child(uid).observeSingleEvent(of: .value) { (ss) in
                    let messageValues = ss.value as! NSDictionary
                    //print(messageValues)
                    let msg = Message(text: messageValues.value(forKey: "text") as! String,
                                      toID: messageValues.value(forKey: "toID") as! String,
                                      fromID: messageValues.value(forKey: "fromID") as! String,
                                      date: messageValues.value(forKey: "date") as! TimeInterval)
                    messages.append(msg)
                    if (messages.count == allIDs.count) {
                        ConversationManager.currentChatLog = messages
                        ConversationManager.cleanChatLog()
                    }
                }
            }
        }
    }
    
    static func getEmailFromUID(uid: String, completion: @escaping (String) -> ()) {
        ref.child("users").child(uid).observeSingleEvent(of: .value) { (snapshot) in
            let values = snapshot.value as! NSDictionary
            //print(values)
            let email = values.value(forKey: "email") as! String
            completion(email)
        }
    }
    
    static var blacwCount = 0
    @objc
    func beginLoadingActiveConversationsWrapper() {
        FirebaseAPIService.blacwCount += 1
        FirebaseAPIService.beginLoadingActiveConversations()
        print("Begin Loading Active Conversations Wrapper Count: \(FirebaseAPIService.blacwCount)")
    }
    
    static var blacCount = 0
    static func beginLoadingActiveConversations() {
        blacCount += 1
        getAllMessageIDs()
        print("Begin Loading Active Conversations Count: \(blacCount)")
    }
    
    static var gamidsCount = 0
    static func getAllMessageIDs() {
        gamidsCount += 1
        let messagesRef = ref.child("messages")
        messagesRef.observeSingleEvent(of: .value) { (snapshot) in
            let values = snapshot.value as! NSDictionary
            ConversationManager.allMessageIDs = values.allKeys as? [String]
            loadAllMessages()
        }
        print("Get All Message IDs Count: \(gamidsCount)")
    }
    
    @objc
    static func loadAllMessages() {
        var allMessages: [Message] = []
        for uid in ConversationManager.allMessageIDs! {
            FirebaseAPIService.ref.child("messages").child(uid).observeSingleEvent(of: .value) { (ss) in
                let messageValues = ss.value as! NSDictionary
                //print(messageValues)
                let msg = Message(text: messageValues.value(forKey: "text") as! String,
                                  toID: messageValues.value(forKey: "toID") as! String,
                                  fromID: messageValues.value(forKey: "fromID") as! String,
                                  date: messageValues.value(forKey: "date") as! TimeInterval)
                allMessages.append(msg)
                if allMessages.count == ConversationManager.allMessageIDs!.count {
                    loadActiveConversations(allMessages: allMessages)
                }
            }
        }
    }
    
    static var lacCount = 0
    static func loadActiveConversations(allMessages: [Message]) {
        lacCount += 1
        var messages = allMessages
        messages.sort(by: { (msg1, msg2) -> Bool in
            return msg1.date < msg2.date
        })
        var activeConversations: [String: Message] = [:]
        for message in messages {
            
            let from = message.fromID
            let to = message.toID
            let my = FirebaseAPIService.currentLocalUser!.uid
            
            if from == my && to != my {
                activeConversations[to] = message
            } else if to == my && from != my {
                activeConversations[from] = message
            } else if to == from {
                continue
            } else {
                continue
            }
        }
        ConversationManager.activeConversations = Array(activeConversations.values)
        NotificationCenter.default.post(Notification(name: Notification.Name("updatedConversations")))
        print("Load Active Conversations Call Count: \(lacCount)")
    }
}
