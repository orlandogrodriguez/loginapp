//
//  ConversationManager.swift
//  LoginApp
//
//  Created by Orlando G. Rodriguez on 11/25/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import Foundation

struct ConversationManager {
    static var allMessageIDs: [String]? {
        didSet {
//            NotificationCenter.default.post(Notification(name: Notification.Name("allMessageIDsObtained")))
        }
    }
    static var activeConversations: [Message]? {
        didSet {
            print("Updated conversations.")
        }
    }
    static var currentParticipants: [LocalUser] = [] {
        didSet {
            print("current participants: \(currentParticipants)")
        }
    }
    static var currentChatLog: [Message] = [] {
        didSet {
            // print(currentChatLog)
            // cleanChatLog()
        }
    }
    static var cleanedChatLog: [Message] = [] {
        didSet {
            NotificationCenter.default.post(Notification(name: Notification.Name("updatedChatLog")))
        }
    }
    
    static func cleanChatLog() {
        var i = 0
        for message in ConversationManager.currentChatLog {
            if (message.toID == ConversationManager.currentParticipants[0].uid ||
                message.toID == ConversationManager.currentParticipants[1].uid) &&
               (message.fromID == ConversationManager.currentParticipants[0].uid ||
                message.fromID == ConversationManager.currentParticipants[1].uid) {
                //print("message was kept: \(message.text)")
            } else {
                //print("message was dropped: \(message.text)")
                ConversationManager.currentChatLog.remove(at: i)
                i -= 1
            }
            i += 1
        }
        cleanedChatLog = ConversationManager.currentChatLog
    }
    static func cleanConversationManager() {
        allMessageIDs = []
        activeConversations = []
        currentParticipants = []
    }
}
