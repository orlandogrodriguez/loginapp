//
//  AppDelegate.swift
//  LoginApp
//
//  Created by Orlando G. Rodriguez on 11/11/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Configure Firebase
        FirebaseApp.configure()
        FirebaseConfiguration.shared.setLoggerLevel(.error)
        
        // Programatically create the first VC and nav controller
        window = UIWindow(frame: UIScreen.main.bounds)
        let mainViewController = SplashViewController()
        let navigationController = UINavigationController(rootViewController: mainViewController)
        navigationController.isNavigationBarHidden = true
        mainViewController.view.backgroundColor = UIColor.white
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        
        return true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        print("Application will enter foreground...")
        guard let user = Auth.auth().currentUser else {
            print("There is no current user logged in.")
            return
        }
        user.reload(completion: nil)
        if user.isEmailVerified {
            print("User has already confirmed their email address...")
            print("Posting notification")
            NotificationCenter.default.post(name: Notification.Name(rawValue: "animateFadeOutConfirmationEmailUIElements"), object: nil)
        } else {
            print("User has not verified email address...")
        }
    }

}

