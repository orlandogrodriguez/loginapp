//
//  ViewController.swift
//  LoginApp
//
//  Created by Orlando G. Rodriguez on 11/11/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController {
    
    // MARK: - Models
    let firebaseAPIService = FirebaseAPIService()
    
    // MARK: - Outlets
    var oGreetingLabel: UILabel = {
        let label = UILabel()
        label.text = "Hello"
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 34)
        label.textAlignment = .center
        label.alpha = 0.0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    var oLoginView: UIView = {
        let view = UIView(frame: CGRect())
        view.backgroundColor = .white
        view.alpha = 0.0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    var oLoginLabel: UILabel = {
        let label = UILabel()
        label.text = "Login"
        label.font = UIFont.boldSystemFont(ofSize: 34)
        label.textAlignment = .left
        label.textColor = Colors.red
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    var oEmailTextField: UITextField = {
        let textField = UITextField(frame: CGRect())
        textField.layer.cornerRadius = 8
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "email"
        textField.autocorrectionType = UITextAutocorrectionType.no
        textField.autocapitalizationType = .none
        textField.keyboardType = .emailAddress
        textField.textContentType = UITextContentType(rawValue: "")
        return textField
    }()
    var oPasswordTextField: UITextField = {
        let textField = UITextField(frame: CGRect())
        textField.layer.cornerRadius = 8
        textField.isSecureTextEntry = true
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "password"
        textField.textContentType = UITextContentType(rawValue: "")
        return textField
    }()
    var oStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = 8
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .center
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    var oGoButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = Colors.purple
        button.setTitle("GO", for: .normal)
        button.addTarget(self, action: #selector(goButtonPressed), for: .touchUpInside)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    var oForgotPasswordButton: UIButton = {
        let button = UIButton()
        button.setTitle("Forgot your password?", for: .normal)
        button.addTarget(self, action: #selector(forgotPasswordPressed), for: .touchUpInside)
        button.setTitleColor(.lightGray, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    var oPlusButton: UIButton = {
        let button = UIButton()
        button.setTitle("", for: .normal)
        button.setImage(UIImage(named: "plus"), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(plusButtonPressed), for: .touchUpInside)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        button.backgroundColor = Colors.red
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    // MARK: - Animatable Constraints
    var yAnchor: NSLayoutConstraint?
    
    var xAnchorPlusButton: NSLayoutConstraint?
    var yAnchorPlusButton: NSLayoutConstraint?
    
    var xAnchorPlusButtonToCenter: NSLayoutConstraint?
    var yAnchorPlusButtonToCenter: NSLayoutConstraint?
    
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //setupUI()
        //setupNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        setupUI()
        setupNotifications()
    }

    func setupUI() {
        
        // Create gradient background
        addGradientBackground()
        
        // Start UI element addition chain
        addLoginView()
        //addGreetingLabel()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(animateMovePlusButtonToTopRightCorner), name: Notification.Name(rawValue: "animateMovePlusButtonToTopRightCorner"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(animateFadeOutAllLoginViewElements), name: Notification.Name(rawValue: "animateFadeOutAllLoginViewElements"), object: nil)
    }
    
    // MARK: - UI Element Addition Functions
    func addGradientBackground() {
        let layer = CAGradientLayer()
        layer.frame = self.view.bounds
        layer.colors = Colors.intro.gradient()
        self.view.layer.addSublayer(layer)
    }
    func addGreetingLabel() {
        
        self.view.addSubview(oGreetingLabel)
        addGreetingLabelConstraints()
        
        // Fade Label In, then back out
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.oGreetingLabel.alpha = 1.0
        }) { (true) in
            UIView.animate(withDuration: 1.0, delay: 2.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.oGreetingLabel.alpha = 0.0
            }, completion: { (true) in
                self.addLoginView()
            })
        }
    }
    func addLoginView() {
        
        // Add login view elements
        self.view.addSubview(oLoginView)
        addEmailPasswordTextFields()
        addLoginViewConstraints()
        addPlusButton()
        // Lay out elements before animation
        self.view.layoutIfNeeded()
        
        // Fade login view in with a subtle upward motion
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.oLoginView.alpha = 1.0
            self.yAnchor?.constant = -22
            self.view.layoutIfNeeded()
        }) { (true) in
            
        }
    }
    func addEmailPasswordTextFields() {
        // Add elements to stack view
        oStackView.addArrangedSubview(oLoginLabel)
        oStackView.addArrangedSubview(oEmailTextField)
        oStackView.addArrangedSubview(oPasswordTextField)
        oStackView.addArrangedSubview(oGoButton)
        oStackView.addArrangedSubview(oForgotPasswordButton)
        
        oEmailTextField.delegate = self
        oEmailTextField.tag = 0
        oEmailTextField.textContentType = .init(rawValue: "")
        createEmailTextFieldToolbar()
        oPasswordTextField.delegate = self
        oPasswordTextField.tag = 1
        oPasswordTextField.textContentType = .init(rawValue: "")
        createPasswordTextFieldToolbar()
        
        // Add stack view to main view
        self.view.addSubview(oStackView)
        
        // Add constraints
        addLoginLabelConstraints()
        addEmailTextFieldConstraints()
        addPasswordTextFieldConstraints()
        addGoButtonConstraints()
        addForgotPasswordButtonConstraints()
        addStackViewConstraints()
    }
    func addPlusButton() {
        self.view.addSubview(oPlusButton)
        addPlusButtonConstraints()
    }
    
    // MARK: - Constraint Functions
    func addGreetingLabelConstraints() {
        oGreetingLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        oGreetingLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        oGreetingLabel.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        oGreetingLabel.heightAnchor.constraint(equalToConstant: 44).isActive = true
    }
    func addStackViewConstraints() {
        oStackView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        oStackView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 64).isActive = true
        oStackView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -64).isActive = true
        oStackView.heightAnchor.constraint(equalToConstant: 320).isActive = true
        self.yAnchor = oStackView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 22)
        yAnchor?.isActive = true
        oStackView.setCustomSpacing(32, after: oPasswordTextField)
    }
    func addLoginLabelConstraints() {
        oLoginLabel.heightAnchor.constraint(equalToConstant: 88).isActive = true
        oLoginLabel.widthAnchor.constraint(equalTo: oStackView.widthAnchor, constant: -32).isActive = true
    }
    func addEmailTextFieldConstraints() {
        oEmailTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        oEmailTextField.widthAnchor.constraint(equalTo: oStackView.widthAnchor, constant: -32).isActive = true
    }
    func addPasswordTextFieldConstraints() {
        oPasswordTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        oPasswordTextField.widthAnchor.constraint(equalTo: oStackView.widthAnchor, constant: -32).isActive = true
    }
    func addGoButtonConstraints() {
        oGoButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        oGoButton.widthAnchor.constraint(equalTo: oStackView.widthAnchor, constant: -128).isActive = true
        oGoButton.layer.cornerRadius = 22
        oGoButton.clipsToBounds = true
    }
    func addForgotPasswordButtonConstraints() {
        oForgotPasswordButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        oForgotPasswordButton.widthAnchor.constraint(equalTo: oStackView.widthAnchor).isActive = true
    }
    func addLoginViewConstraints() {
        // Layer properties
        oLoginView.layer.cornerRadius = 16
        oLoginView.leftAnchor.constraint(equalTo: oStackView.leftAnchor, constant: -8).isActive = true
        oLoginView.rightAnchor.constraint(equalTo: oStackView.rightAnchor, constant: 8).isActive = true
        oLoginView.topAnchor.constraint(equalTo: oStackView.topAnchor, constant: -8).isActive = true
        oLoginView.bottomAnchor.constraint(equalTo: oStackView.bottomAnchor, constant: 8).isActive = true
    }
    func addPlusButtonConstraints() {
        oPlusButton.layer.cornerRadius = 32
        xAnchorPlusButton = oPlusButton.centerXAnchor.constraint(equalTo: oLoginView.rightAnchor)
        xAnchorPlusButton?.isActive = true
        yAnchorPlusButton = oPlusButton.centerYAnchor.constraint(equalTo: oLoginLabel.centerYAnchor)
        yAnchorPlusButton?.isActive = true
        
        oPlusButton.widthAnchor.constraint(equalToConstant: 64).isActive = true
        oPlusButton.heightAnchor.constraint(equalToConstant: 64).isActive = true
    }
    
    // MARK: - Animation Functions
    func animateMovePlusButtonToCenter() {
        
        xAnchorPlusButton?.isActive = false
        yAnchorPlusButton?.isActive = false
        
        xAnchorPlusButtonToCenter = self.oPlusButton.centerXAnchor.constraint(equalTo: self.oLoginView.centerXAnchor)
        yAnchorPlusButtonToCenter = self.oPlusButton.centerYAnchor.constraint(equalTo: self.oLoginView.centerYAnchor)
        
        UIView.animate(withDuration: 0.25, delay: 0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.xAnchorPlusButtonToCenter?.isActive = true
            self.yAnchorPlusButtonToCenter?.isActive = true
            self.view.layoutSubviews()
        }) { (true) in
            // Create the next view controller
            let nextVC = RegisterViewController()
            nextVC.previousVC = self
            nextVC.view.backgroundColor = .clear
            nextVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(nextVC, animated: false, completion: nil)
        }
    }
    @objc
    func animateMovePlusButtonToTopRightCorner() {
        xAnchorPlusButtonToCenter?.isActive = false
        yAnchorPlusButtonToCenter?.isActive = false
        UIView.animate(withDuration: 0.25, delay: 0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.xAnchorPlusButton?.isActive = true
            self.yAnchorPlusButton?.isActive = true
            self.view.layoutSubviews()
        }) { (true) in
            
        }
    }
    @objc
    func animateFadeOutAllLoginViewElements() {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.oLoginView.alpha = 0.0
            self.oLoginLabel.alpha = 0.0
            self.oEmailTextField.alpha = 0.0
            self.oPasswordTextField.alpha = 0.0
            self.oForgotPasswordButton.alpha = 0.0
            self.oGoButton.alpha = 0.0
            self.oPlusButton.alpha = 0.0
        }) { (true) in
            guard let user = Auth.auth().currentUser else { return }
            if self.firebaseAPIService.userDidConfirmEmail(user: user) {
                let nextVC: UIViewController = WelcomeViewController()
                self.navigationController?.pushViewController(nextVC, animated: false)
            } else {
                let nextVC: UIViewController = ConfirmEmailViewController()
                self.navigationController?.pushViewController(nextVC, animated: false)
            }
        }
        
    }
    
    func clearLoginView() {
        self.oLoginView.alpha = 0.0
        self.oLoginLabel.alpha = 0.0
        self.oEmailTextField.alpha = 0.0
        self.oPasswordTextField.alpha = 0.0
        self.oForgotPasswordButton.alpha = 0.0
        self.oGoButton.alpha = 0.0
        self.oPlusButton.alpha = 0.0
    }
    
    // MARK: - Actions
    @objc
    func goButtonPressed() {
        dismissKeyboard()
        guard let email = oEmailTextField.text,
              let password = oPasswordTextField.text else {
                let alert = UIAlertController(title: "Invalid Email or Password", message: "The email and password combination you provided was invalid. Please try again.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                return
        }
        firebaseAPIService.login(with: email, password)
    }
    @objc
    func forgotPasswordPressed() {
        
    }
    
    @objc
    func plusButtonPressed() {
        animateMovePlusButtonToCenter()
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
    }
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            view.endEditing(true)
        }
        
        return true
    }
    
    func createEmailTextFieldToolbar() {
        let bar = UIToolbar()
        let continueButton = UIBarButtonItem(title: "CONTINUE", style: .plain, target: self, action: #selector(makePasswordTextFieldFirstResponder))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        bar.items = [flexibleSpace, continueButton, flexibleSpace]
        bar.tintColor = .white
        bar.barTintColor = UIColor.init(rgb: 0x2ecc71)
        bar.sizeToFit()
        oEmailTextField.inputAccessoryView = bar
    }
    
    func createPasswordTextFieldToolbar() {
        let bar = UIToolbar()
        let signInButton = UIBarButtonItem(title: "SIGN IN", style: .plain, target: self, action: #selector(goButtonPressed))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        bar.items = [flexibleSpace, signInButton, flexibleSpace]
        bar.tintColor = .white
        bar.barTintColor = UIColor.init(rgb: 0x2ecc71)
        bar.sizeToFit()
        oPasswordTextField.inputAccessoryView = bar
    }
    
    @objc
    func makePasswordTextFieldFirstResponder() {
        oPasswordTextField.becomeFirstResponder()
    }
    
    @objc
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
