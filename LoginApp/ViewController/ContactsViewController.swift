//
//  ContactsViewController.swift
//  LoginApp
//
//  Created by Orlando G. Rodriguez on 11/22/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import UIKit
import Firebase

class ContactsViewController: UIViewController {
    var prevVC: MessagesViewController?
    lazy var oContactsTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupNotifications()
    }
    
    func setupUI() {
        addContactsTableView()
        guard let navController = navigationController else {
            print("No navigation bar available. Fix this bug.")
            return
        }
        navController.navigationBar.topItem?.title = "Contacts"
    }
    
    @objc func updateViewFromModel() {
        print("Updating view from model.")
        oContactsTableView.reloadData()
    }
    
    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateViewFromModel), name: Notification.Name(rawValue: "usersUpdated"), object: nil)
    }
    
    func addContactsTableView() {
        view.addSubview(oContactsTableView)
        addContactsTableViewConstraints()
    }
    
    func addContactsTableViewConstraints() {
        oContactsTableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        oContactsTableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        oContactsTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        oContactsTableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    }

}

extension ContactsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FirebaseService.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "contactCell")
        var users: [LocalUser] = []
        users = Array(FirebaseService.users.values).sorted(by: { (user1, user2) -> Bool in
            return user1.email < user2.email
        })
        
        if users.count == 0 {
            print("No users available.")
            return cell
        }
        cell.textLabel?.text = users[indexPath.row].email
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        guard let navController = navigationController else {
//            print("No navigation controller available. Fix this bug.")
//            return
//        }
        tableView.deselectRow(at: indexPath, animated: true)
//        ConversationManager.currentParticipants = [FirebaseAPIService.allUsers[indexPath.row], FirebaseAPIService.currentLocalUser!]
//        ConversationManager.currentChatLog = []
        // navController.pushViewController(nextVC, animated: true)
        
        dismiss(animated: true) {
            guard let navController = self.prevVC!.navigationController else {
                print("navigation controller not found.")
                return
            }
//            let nextVC = ConversationViewController()
//            let receiverID = FirebaseAPIService.allUsers[indexPath.row].uid
//            let receiver = LocalUser(uid: receiverID)
//            ConversationManager.currentParticipants = ([receiver, FirebaseAPIService.currentLocalUser] as? [LocalUser])!
//            let nextVC = ConversationViewController()
//            let conversationEmails = FirebaseService.conversations.keys.sorted { (str1, str2) -> Bool in
//                return str1 > str2
//            }
//            var conversationMessages: [Message] = []
//            for email in conversationEmails {
//                conversationMessages.append(FirebaseService.conversations[email]!)
//            }
            
            //if conversationMessages.count == 0 {
            var users: [LocalUser] = []
            users = Array(FirebaseService.users.values).sorted(by: { (user1, user2) -> Bool in
                return user1.email < user2.email
            })
            
            if users.count == 0 {
                print("No users available.")
                return
            }
            
            let nextVC = ConversationViewController()
            let receiver = users[indexPath.row]
            guard let sender = FirebaseService.currentLocalUser else { return }
            nextVC.participants = [receiver, sender]
                
                
                
//            } else {
//
//                let currentUserID = FirebaseService.currentLocalUser?.uid
//                var receiverID: String
//                if conversationMessages[indexPath.row].toID == currentUserID {
//                    receiverID = conversationMessages[indexPath.row].fromID
//                } else {
//                    receiverID = conversationMessages[indexPath.row].toID
//                }
//                let receiver = LocalUser(uid: receiverID, email: conversationEmails[indexPath.row])
//                let sender = FirebaseService.currentLocalUser!
//                nextVC.participants = [receiver, sender]
//            }
            navController.pushViewController(nextVC, animated: true)
        }
    }
    
}
