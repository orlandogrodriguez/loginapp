//
//  RegisterViewController.swift
//  LoginApp
//
//  Created by Orlando G. Rodriguez on 11/14/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import UIKit
import FirebaseAuth

class RegisterViewController: UIViewController {
    
    // MARK: - Models
    let firebaseAPIService = FirebaseAPIService()
    weak var previousVC: LoginViewController?
    
    // MARK: - Outlets
    var oRegisterView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.red
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    var oRegisterLabel: UILabel = {
        let label = UILabel()
        label.alpha = 0
        label.text = "Register"
        label.font = UIFont.boldSystemFont(ofSize: 34)
        label.textAlignment = .left
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    var oEmailTextField: UITextField = {
        let textField = UITextField(frame: CGRect())
        textField.alpha = 0
        textField.layer.cornerRadius = 8
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.attributedPlaceholder = NSAttributedString(string: "email", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white.withAlphaComponent(0.5)])
        textField.autocorrectionType = UITextAutocorrectionType.no
        textField.autocapitalizationType = .none
        textField.keyboardType = .emailAddress
        textField.textContentType = UITextContentType(rawValue: "")
        textField.textColor = .white
        return textField
    }()
    var oPasswordTextField: UITextField = {
        let textField = UITextField(frame: CGRect())
        textField.alpha = 0
        textField.layer.cornerRadius = 8
        textField.isSecureTextEntry = true
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.attributedPlaceholder = NSAttributedString(string: "password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white.withAlphaComponent(0.5)])
        textField.textContentType = UITextContentType(rawValue: "")
        textField.textColor = .white
        return textField
    }()
    var oRepeatPasswordTextField: UITextField = {
        let textField = UITextField(frame: CGRect())
        textField.alpha = 0
        textField.layer.cornerRadius = 8
        textField.isSecureTextEntry = true
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.attributedPlaceholder = NSAttributedString(string: "confirm password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white.withAlphaComponent(0.5)])
        textField.textContentType = UITextContentType(rawValue: "")
        textField.textColor = .white
        return textField
    }()
    var oStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .center
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    var oNextButton: UIButton = {
        let button = UIButton()
        button.alpha = 0
        button.backgroundColor = .white
        button.setTitle("NEXT", for: .normal)
        button.setTitleColor(Colors.red, for: .normal)
        button.addTarget(self, action: #selector(nextButtonPressed), for: .touchUpInside)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    var oXButton: UIButton = {
        let button = UIButton()
        button.setTitle("", for: .normal)
        button.setImage(UIImage(named: "plus"), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(xButtonPressed), for: .touchUpInside)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        button.backgroundColor = Colors.red
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    var oSpacer: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 8))
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK: - Animatable Constraints
    var aRegisterViewWidth64: NSLayoutConstraint?
    var aRegisterViewHeight64: NSLayoutConstraint?
    var aRegisterViewWidthToStackViewWidth: NSLayoutConstraint?
    var aRegisterViewHeightToStackViewHeight: NSLayoutConstraint?
    
    var aXButtonCenterXToViewCenterX: NSLayoutConstraint?
    var aXButtonCenterYToViewCenterY: NSLayoutConstraint?
    var aXButtonRightToStackViewRight: NSLayoutConstraint?
    var aXButtonTopToStackViewTop: NSLayoutConstraint?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupNotifications()
    }
    
    override func viewDidLayoutSubviews() {
        animateMoveXButtonToTopRightCorner()
        animateStretchRegisterViewFromCircleToRectangle()
        animateFadeInRegisterUIElements()
    }
    
    func setupUI() {
        addRegisterView()
        addRegisterElements()
        addXButton()
        addGestureRecognizers()
        animateRotateXButton()
    }
    
    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(animateFadeOutRegisterViewElements), name: Notification.Name(rawValue: "animateFadeOutRegisterViewElements"), object: nil)
    }
    
    // MARK: - UI Element Addition Functions
    func addRegisterView() {
        oRegisterView.layer.cornerRadius = 32
        view.addSubview(oRegisterView)
        addRegisterViewConstraints()
    }
    func addRegisterElements() {
        addStackView()
        oEmailTextField.delegate = self
        oEmailTextField.tag = 0
        oPasswordTextField.delegate = self
        oPasswordTextField.tag = 1
        oRepeatPasswordTextField.delegate = self
        oRepeatPasswordTextField.tag = 2
        createEmailTextFieldToolbar()
        createPasswordTextFieldToolbar()
    }
    func addStackView() {
        oStackView.addArrangedSubview(oRegisterLabel)
        oStackView.addArrangedSubview(oEmailTextField)
        oStackView.addArrangedSubview(oPasswordTextField)
        oStackView.addArrangedSubview(oRepeatPasswordTextField)
        oStackView.addArrangedSubview(oNextButton)
        oStackView.addArrangedSubview(oSpacer)
        view.addSubview(oStackView)
        addStackViewConstraints()
    }
    func addXButton() {
        oXButton.layer.cornerRadius = 32
        view.addSubview(oXButton)
        addXButtonConstraints()
    }
    func addGestureRecognizers() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    // MARK: - Constraint Functions
    func addRegisterViewConstraints() {
        oRegisterView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        oRegisterView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -22).isActive = true
        aRegisterViewWidth64 =  oRegisterView.widthAnchor.constraint(equalToConstant: 64)
        aRegisterViewHeight64 = oRegisterView.heightAnchor.constraint(equalToConstant: 64)
        aRegisterViewWidthToStackViewWidth = oRegisterView.widthAnchor.constraint(equalTo: oStackView.widthAnchor, constant: 16)
        aRegisterViewHeightToStackViewHeight = oRegisterView.heightAnchor.constraint(equalTo: oStackView.heightAnchor, constant: 16)
        aRegisterViewWidth64?.isActive = true
        aRegisterViewHeight64?.isActive = true
        aRegisterViewHeightToStackViewHeight?.isActive = false
        aRegisterViewWidthToStackViewWidth?.isActive = false
    }
    func addStackViewConstraints() {
        oStackView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        oStackView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 64).isActive = true
        oStackView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -64).isActive = true
        oStackView.heightAnchor.constraint(equalToConstant: 320).isActive = true
        oStackView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -22).isActive = true

        addRegisterLabelConstraints()
        addEmailTextFieldConstraints()
        addPasswordTextFieldConstraints()
        addRepeatPasswordTextFieldConstraints()
        addNextButtonConstraints()
    }
    func addRegisterLabelConstraints() {
        oRegisterLabel.heightAnchor.constraint(equalToConstant: 64).isActive = true
        oRegisterLabel.widthAnchor.constraint(equalTo: oStackView.widthAnchor, constant: -32).isActive = true
    }
    func addEmailTextFieldConstraints() {
        oEmailTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        oEmailTextField.widthAnchor.constraint(equalTo: oStackView.widthAnchor, constant: -32).isActive = true
    }
    func addPasswordTextFieldConstraints() {
        oPasswordTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        oPasswordTextField.widthAnchor.constraint(equalTo: oStackView.widthAnchor, constant: -32).isActive = true
    }
    func addRepeatPasswordTextFieldConstraints() {
        oRepeatPasswordTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        oRepeatPasswordTextField.widthAnchor.constraint(equalTo: oStackView.widthAnchor, constant: -32).isActive = true
    }
    func addNextButtonConstraints() {
        oNextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        oNextButton.widthAnchor.constraint(equalTo: oStackView.widthAnchor, constant: -128).isActive = true
        oNextButton.bottomAnchor.constraint(equalTo: oStackView.bottomAnchor, constant: -16).isActive = true
        oNextButton.layer.cornerRadius = 22
        oNextButton.clipsToBounds = true
    }
    func addXButtonConstraints() {
        aXButtonCenterXToViewCenterX = oXButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        aXButtonCenterYToViewCenterY = oXButton.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -22)
        
        aXButtonTopToStackViewTop = oXButton.topAnchor.constraint(equalTo: oStackView.topAnchor)
        aXButtonRightToStackViewRight = oXButton.rightAnchor.constraint(equalTo: oStackView.rightAnchor)
        
        aXButtonCenterXToViewCenterX?.isActive = true
        aXButtonCenterYToViewCenterY?.isActive = true
        
        aXButtonTopToStackViewTop?.isActive = false
        aXButtonRightToStackViewRight?.isActive = false
        
        oXButton.widthAnchor.constraint(equalToConstant: 64).isActive = true
        oXButton.heightAnchor.constraint(equalToConstant: 64).isActive = true
    }
    
    // Animation Functions
    func animateRotateXButton() {
        let buttonRotation = CAKeyframeAnimation(keyPath: "transform.rotation")
        buttonRotation.keyTimes = [0, 1]
        buttonRotation.values = [0, Double.pi / 4]
        buttonRotation.duration = 0.25
        buttonRotation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        buttonRotation.fillMode = CAMediaTimingFillMode.forwards
        buttonRotation.isRemovedOnCompletion = false
        
        oXButton.layer.add(buttonRotation, forKey: "transform.rotation")
    }
    func animateReverseRotateXButton() {
        let buttonRotation = CAKeyframeAnimation(keyPath: "transform.rotation")
        buttonRotation.keyTimes = [0, 1]
        buttonRotation.values = [Double.pi / 4, 0]
        buttonRotation.duration = 0.25
        buttonRotation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        buttonRotation.fillMode = CAMediaTimingFillMode.forwards
        buttonRotation.isRemovedOnCompletion = false
        
        oXButton.layer.add(buttonRotation, forKey: "transform.rotation")
    }
    func animateMoveXButtonToTopRightCorner() {
        aXButtonCenterXToViewCenterX?.isActive = false
        aXButtonCenterYToViewCenterY?.isActive = false
        UIView.animate(withDuration: 0.25, delay: 0.25, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.aXButtonRightToStackViewRight?.isActive = true
            self.aXButtonTopToStackViewTop?.isActive = true
            self.view.layoutSubviews()
        }) { (true) in
            
        }
    }
    func animateMoveXButtonToCenter() {
        self.aXButtonRightToStackViewRight?.isActive = false
        self.aXButtonTopToStackViewTop?.isActive = false
        UIView.animate(withDuration: 0.25, delay: 0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.aXButtonCenterXToViewCenterX?.isActive = true
            self.aXButtonCenterYToViewCenterY?.isActive = true
            self.view.layoutSubviews()
        }) { (true) in
            
        }
    }
    func animateStretchRegisterViewFromCircleToRectangle() {
        aRegisterViewWidth64?.isActive = false
        aRegisterViewHeight64?.isActive = false
        UIView.animate(withDuration: 0.25, delay: 0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.aRegisterViewHeightToStackViewHeight?.isActive = true
            self.aRegisterViewWidthToStackViewWidth?.isActive = true
            self.oRegisterView.layer.cornerRadius = 16
            self.view.layoutSubviews()
        }) { (true) in
            
        }
    }
    func animateStretchRegisterViewFromRectangleToCircle() {
        self.aRegisterViewHeightToStackViewHeight?.isActive = false
        self.aRegisterViewWidthToStackViewWidth?.isActive = false
        
        UIView.animate(withDuration: 0.25, delay: 0.25, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.aRegisterViewWidth64?.isActive = true
            self.aRegisterViewHeight64?.isActive = true
            self.oRegisterView.layer.cornerRadius = 32
            self.view.layoutSubviews()
        }) { (true) in
            self.dismiss(animated: false, completion: {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "animateMovePlusButtonToTopRightCorner"), object: nil)
            })
            
        }
    }
    func animateFadeInRegisterUIElements() {
        UIView.animate(withDuration: 0.25, delay: 0.25, options: .curveEaseInOut, animations: {
            self.oRegisterLabel.alpha = 1.0
            self.oEmailTextField.alpha = 1.0
            self.oPasswordTextField.alpha = 1.0
            self.oRepeatPasswordTextField.alpha = 1.0
            self.oNextButton.alpha = 1.0
        }) { (true) in
            
        }
    }
    @objc func animateFadeOutStackViewElements() {
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
            self.oRegisterLabel.alpha = 0.0
            self.oEmailTextField.alpha = 0.0
            self.oPasswordTextField.alpha = 0.0
            self.oRepeatPasswordTextField.alpha = 0.0
            self.oNextButton.alpha = 0.0
        }) { (true) in

        }
    }
    @objc func animateFadeOutRegisterViewElements() {
        previousVC?.clearLoginView()
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
            self.oRegisterLabel.alpha = 0.0
            self.oEmailTextField.alpha = 0.0
            self.oPasswordTextField.alpha = 0.0
            self.oRepeatPasswordTextField.alpha = 0.0
            self.oNextButton.alpha = 0.0
            self.oRegisterView.alpha = 0.0
            self.oXButton.alpha = 0.0
        }) { (true) in
            guard Auth.auth().currentUser != nil else { return }
            NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "animateFadeOutAllLoginViewElements"), object: nil))
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    // MARK: - Actions
    @objc
    func xButtonPressed() {
        animateReverseRotateXButton()
        animateMoveXButtonToCenter()
        animateFadeOutStackViewElements()
        animateStretchRegisterViewFromRectangleToCircle()
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
    }
    
    @objc
    func nextButtonPressed() {
        dismissKeyboard()
        guard let email = oEmailTextField.text,
            let password = oPasswordTextField.text,
            let repeatPassword = oRepeatPasswordTextField.text else {
                let alert = UIAlertController(title: "Invalid Email/Password Combination", message: "The email/password combination you provided is invalid. Please try again.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                present(alert, animated: true)
                return
        }
        firebaseAPIService.signup(with: email, password: password, repeatPassword: repeatPassword)
    }
}

extension RegisterViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            view.endEditing(true)
        }
        
        return true
    }
    
    func createEmailTextFieldToolbar() {
        let bar = UIToolbar()
        let continueButton = UIBarButtonItem(title: "CONTINUE", style: .plain, target: self, action: #selector(makePasswordTextFieldFirstResponder))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        bar.items = [flexibleSpace, continueButton, flexibleSpace]
        bar.tintColor = .white
        bar.barTintColor = UIColor.init(rgb: 0x2ecc71)
        bar.sizeToFit()
        oEmailTextField.inputAccessoryView = bar
        oPasswordTextField.inputAccessoryView = bar
    }
    
    func createPasswordTextFieldToolbar() {
        let bar = UIToolbar()
        let signInButton = UIBarButtonItem(title: "SIGN UP", style: .plain, target: self, action: #selector(nextButtonPressed))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        bar.items = [flexibleSpace, signInButton, flexibleSpace]
        bar.tintColor = .white
        bar.barTintColor = UIColor.init(rgb: 0x2ecc71)
        bar.sizeToFit()
        oRepeatPasswordTextField.inputAccessoryView = bar
    }
    
    @objc
    func makePasswordTextFieldFirstResponder() {
        oPasswordTextField.becomeFirstResponder()
    }
    
    @objc
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
