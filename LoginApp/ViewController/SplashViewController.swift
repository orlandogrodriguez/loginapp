//
//  SplashViewController.swift
//  LoginApp
//
//  Created by Orlando G. Rodriguez on 11/15/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import UIKit
import FirebaseAuth

class SplashViewController: UIViewController {

    // MARK: - Outlets
    var oHelloLabel: UILabel = {
        let label = UILabel()
        label.text = "Hello"
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 34)
        label.textAlignment = .center
        label.alpha = 0.0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        animateFadeInWelcomeLabel()
    }
    
    func setupUI() {
        addGradientBackground()
        addWelcomeLabel()
    }
    
    // MARK: - UI Element Addition Functions
    func addGradientBackground() {
        let layer = CAGradientLayer()
        layer.frame = self.view.bounds
        layer.colors = Colors.intro.gradient()
        self.view.layer.addSublayer(layer)
    }
    func addWelcomeLabel() {
        view.addSubview(oHelloLabel)
        addWelcomeLabelConstraints()
    }
    
    func addWelcomeLabelConstraints() {
        oHelloLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        oHelloLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        oHelloLabel.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -64).isActive = true
        oHelloLabel.heightAnchor.constraint(equalToConstant: 88).isActive = true
    }
    
    func animateFadeInWelcomeLabel() {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.oHelloLabel.alpha = 1.0
        }) { (true) in
            UIView.animate(withDuration: 0.5, delay: 1.0, options: .curveEaseInOut, animations: {
                self.oHelloLabel.alpha = 0.0
            }) { (true) in
                var nextVC: UIViewController
                if Auth.auth().currentUser != nil {
                    nextVC = WelcomeViewController()
                } else {
                    nextVC = LoginViewController()
                }
                guard let navController = self.navigationController else {
                    print("No navigation controller available.")
                    return
                }
                navController.pushViewController(nextVC, animated: false)
            }
        }
    }

}
