//
//  HomeViewController.swift
//  LoginApp
//
//  Created by Orlando G. Rodriguez on 11/15/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import UIKit
import FirebaseDatabase

class HomeViewController: UIViewController {
    
    // MARK: - Models
    let firebaseAPIService = FirebaseAPIService()
    
    // MARK: - Outlets
    var oEmailLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 34, weight: .light)
        label.textAlignment = .center
        label.alpha = 0.0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    var oMessagesAppButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "messages"), for: .normal)
        button.addTarget(self, action: #selector(messagesAppButtonPressed), for: .touchUpInside)
        button.tintColor = Colors.purple
        button.backgroundColor = Colors.lightGray
        button.translatesAutoresizingMaskIntoConstraints = false
        button.alpha = 0.0
        return button
    }()
    var oAnalyticsAppButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "analytics"), for: .normal)
        button.addTarget(self, action: #selector(analyticsAppButtonPressed), for: .touchUpInside)
        button.tintColor = Colors.purple
        button.backgroundColor = Colors.lightGray
        button.translatesAutoresizingMaskIntoConstraints = false
        button.alpha = 0.0
        return button
    }()
    var oLogoutButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "logout"), for: .normal)
        button.tintColor = .white
        button.backgroundColor = Colors.red
        button.addTarget(self, action: #selector(logoutButtonPressed), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.alpha = 0.0
        return button
    }()
    var oCentralStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = 16
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .center
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    var oButtonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = 32
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupNotifications()
//        Database.database().reference().child("messages").observe(DataEventType.childAdded) { (snapshot) in
//            FirebaseAPIService.beginLoadingActiveConversations()
//        }
        FirebaseService.observeUserChanges {
            FirebaseService.observeMessageChanges()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationControllerProperties()
    }
    
    override func viewDidLayoutSubviews() {
        animateFadeInAllUIElements()
    }
    
    func setupUI() {
        addGradientBackground()
        addStackView()
        addEmailLabel()
        addButtonsStackView()
        addMessagesAppButton()
        addAnalyticsAppButton()
        addLogoutButton()
    }
    
    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(animateFadeOutAllUIElements), name: Notification.Name(rawValue: "userLoggedOut"), object: nil)
    }
    
    // MARK: - UI Element Addition Functions
    func addGradientBackground() {
        let layer = CAGradientLayer()
        layer.frame = self.view.bounds
        layer.colors = Colors.intro.gradient()
        self.view.layer.addSublayer(layer)
    }
    func addStackView() {
        view.addSubview(oCentralStackView)
        addCentralStackViewConstraints()
    }
    func addButtonsStackView() {
        oCentralStackView.addArrangedSubview(oButtonsStackView)
    }
    func addEmailLabel() {
        oEmailLabel.text = FirebaseAPIService.currentUser?.email
        oCentralStackView.addArrangedSubview(oEmailLabel)
    }
    func addMessagesAppButton() {
        oButtonsStackView.addArrangedSubview(oMessagesAppButton)
        oMessagesAppButton.widthAnchor.constraint(equalToConstant: 64).isActive = true
        oMessagesAppButton.heightAnchor.constraint(equalToConstant: 64).isActive = true
        oMessagesAppButton.layer.cornerRadius = 32
    }
    func addAnalyticsAppButton() {
        oButtonsStackView.addArrangedSubview(oAnalyticsAppButton)
        oAnalyticsAppButton.widthAnchor.constraint(equalToConstant: 64).isActive = true
        oAnalyticsAppButton.heightAnchor.constraint(equalToConstant: 64).isActive = true
        oAnalyticsAppButton.layer.cornerRadius = 32
    }
    func addLogoutButton() {
        oButtonsStackView.addArrangedSubview(oLogoutButton)
        oLogoutButton.widthAnchor.constraint(equalToConstant: 64).isActive = true
        oLogoutButton.heightAnchor.constraint(equalToConstant: 64).isActive = true
        oLogoutButton.layer.cornerRadius = 32
    }
    func setupNavigationControllerProperties() {
        guard let navController = navigationController else {
            print("No navigation controller available. Fix this bug.")
            return
        }
        navController.isNavigationBarHidden = true
        navController.isToolbarHidden = true
    }
    
    // MARK: - Constraint Functions
    func addCentralStackViewConstraints() {
        oCentralStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        oCentralStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        oCentralStackView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -64).isActive = true
    }
    
    @objc
    func animateFadeOutAllUIElements() {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.oEmailLabel.alpha = 0.0
            self.oMessagesAppButton.alpha = 0.0
            self.oAnalyticsAppButton.alpha = 0.0
            self.oLogoutButton.alpha = 0.0
        }) { _ in
            self.navigationController?.popToRootViewController(animated: false)
        }
    }
    
    @objc
    func animateFadeInAllUIElements() {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.oEmailLabel.alpha = 1.0
            self.oMessagesAppButton.alpha = 1.0
            self.oAnalyticsAppButton.alpha = 1.0
            self.oLogoutButton.alpha = 1.0
        }) { _ in
            
        }
    }
    
    // MARK: - Actions
    @objc
    func logoutButtonPressed() {
        firebaseAPIService.logoutCurrentUser()
    }
    
    @objc
    func messagesAppButtonPressed() {
        let nextVC = MessagesViewController()
        guard let navController = navigationController else {
            print("Navigation controller not available. Fix this bug.")
            return
        }
        navController.pushViewController(nextVC, animated: true)
    }
    
    @objc
    func analyticsAppButtonPressed() {
        let nextVC = AnalyticsViewController()
        guard let navController = navigationController else { return }
        navController.pushViewController(nextVC, animated: true)
    }

}
