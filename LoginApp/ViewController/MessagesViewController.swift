//
//  MessagesViewController.swift
//  ChatApp
//
//  Created by Orlando G. Rodriguez on 11/20/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import UIKit
import Firebase

class MessagesViewController: UIViewController {
    
    var oConversationsTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.tableFooterView = UIView()
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func setupUI() {
        addNavigationController()
        addConversationsTableView()
        oConversationsTableView.reloadData()
    }
    
    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateViewFromModel), name: NSNotification.Name("updatedConversations"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateViewFromModel), name: NSNotification.Name("loadedAllMessages"), object: nil)
    }
    
    @objc
    func updateViewFromModel() {
        oConversationsTableView.reloadData()
    }
    
    func addNavigationController() {
        guard let navController = navigationController else {
            print("No navigation controller is available. Fix this bug.")
            return
        }
        navController.isToolbarHidden = false
        navController.isNavigationBarHidden = false
        navController.navigationBar.topItem?.title = "Messages"
        navController.navigationBar.backItem?.title = "Home"
        let newMessageButton = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(newMessageButtonPressed))
        navigationItem.rightBarButtonItem = newMessageButton
    }
    
    func addConversationsTableView() {
        oConversationsTableView.delegate = self
        oConversationsTableView.dataSource = self
        oConversationsTableView.register(ConversationCell.self, forCellReuseIdentifier: ConversationCell.reuseIdentifier)
        view.addSubview(oConversationsTableView)
        addConversationsTableViewConstraints()
    }
    
    func addConversationsTableViewConstraints() {
        oConversationsTableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        oConversationsTableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        oConversationsTableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        oConversationsTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    // MARK: - Actions
    @objc
    func newMessageButtonPressed() {
        print("New message button pressed.")
        FirebaseAPIService.getContacts()
        let nextVC = ContactsViewController()
        nextVC.prevVC = self
        self.present(nextVC, animated: true, completion: nil)
    }
}

extension MessagesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FirebaseService.conversations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let conversationEmails = FirebaseService.conversations.keys.sorted { (str1, str2) -> Bool in
            return str1 > str2
        }
        var conversationMessages: [Message] = []
        for email in conversationEmails {
            conversationMessages.append(FirebaseService.conversations[email]!)
        }
        
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "ConversationCell")
        cell.textLabel!.text = conversationEmails[indexPath.row]
        cell.detailTextLabel!.text = conversationMessages[indexPath.row].text
    
//        let cell = tableView.dequeueReusableCell(withIdentifier: ConversationCell.reuseIdentifier, for: indexPath) as! ConversationCell
        //cell.updateModel(conversation: conversationMessages[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let nextVC = ConversationViewController()
        let conversationEmails = FirebaseService.conversations.keys.sorted { (str1, str2) -> Bool in
            return str1 > str2
        }
        var conversationMessages: [Message] = []
        for email in conversationEmails {
            conversationMessages.append(FirebaseService.conversations[email]!)
        }
        
        let currentUserID = FirebaseService.currentLocalUser?.uid
        var receiverID: String
        if conversationMessages[indexPath.row].toID == currentUserID {
            receiverID = conversationMessages[indexPath.row].fromID
        } else {
            receiverID = conversationMessages[indexPath.row].toID
        }
        let receiver = LocalUser(uid: receiverID, email: conversationEmails[indexPath.row])
        let sender = FirebaseService.currentLocalUser!
        nextVC.participants = [receiver, sender]
        self.navigationController?.pushViewController(nextVC, animated: true)
//        guard var receiverID = ConversationManager.activeConversations?[indexPath.row].toID else { return }
//        if receiverID == FirebaseAPIService.currentLocalUser!.uid {
//            receiverID = (ConversationManager.activeConversations?[indexPath.row].fromID)!
//        }
//        let receiver = LocalUser(uid: receiverID)
//        ConversationManager.currentParticipants = ([receiver, FirebaseAPIService.currentLocalUser] as? [LocalUser])!
//        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    
}
