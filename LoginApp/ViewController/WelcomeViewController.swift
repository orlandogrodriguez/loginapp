//
//  WelcomeViewController.swift
//  LoginApp
//
//  Created by Orlando G. Rodriguez on 11/14/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    // MARK: - Models
    let firebaseAPIService = FirebaseAPIService()
    
    // MARK: - Outlets
    var oWelcomeLabel: UILabel = {
        let label = UILabel()
        label.text = "Welcome"
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 34)
        label.textAlignment = .center
        label.alpha = 0.0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if FirebaseAPIService.currentUser == nil {
            print("No user is currently logged in. Dismissing.")
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    override func viewDidLayoutSubviews() {
        animateFadeInWelcomeLabel()
    }
    
    func setupUI() {
        addGradientBackground()
        addWelcomeLabel()
    }
    
    func setupNotifications() {
        
    }
    
    // MARK: - UI Element Addition Functions
    func addGradientBackground() {
        let layer = CAGradientLayer()
        layer.frame = self.view.bounds
        layer.colors = Colors.intro.gradient()
        self.view.layer.addSublayer(layer)
    }
    func addWelcomeLabel() {
        view.addSubview(oWelcomeLabel)
        addWelcomeLabelConstraints()
    }
    
    func addWelcomeLabelConstraints() {
        oWelcomeLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        oWelcomeLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        oWelcomeLabel.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -64).isActive = true
        oWelcomeLabel.heightAnchor.constraint(equalToConstant: 88).isActive = true
    }
    
    func animateFadeInWelcomeLabel() {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.oWelcomeLabel.alpha = 1.0
        }) { (true) in
            UIView.animate(withDuration: 0.5, delay: 1.0, options: .curveEaseInOut, animations: {
                self.oWelcomeLabel.alpha = 0.0
            }) { (true) in
                print("Pushing home view controller.")
                let nextVC = HomeViewController()
                guard let navController = self.navigationController else {
                    print("No navigation controller available.")
                    return
                }
                navController.pushViewController(nextVC, animated: false)
                //self.present(nextVC, animated: false, completion: nil)
            }
        }
    }
    
}
