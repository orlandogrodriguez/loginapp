//
//  AnalyticsViewController.swift
//  LoginApp
//
//  Created by Orlando G. Rodriguez on 11/28/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import UIKit

class AnalyticsViewController: UIViewController {
    
    let chart = BarChart(title: "Fruits In Basket", values: [
        "🍎": 6.0,
        "🍐": 2.0,
        "🍉": 12.0,
        "🍇": 10.0,
        "🍌": 3.0,
        "🍈": 2.0
    ])
    
    var oChartView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var oChartLabelsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .equalCentering
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    var oChartBarsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .equalCentering
        stackView.alignment = .bottom
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        view.backgroundColor = .white
        addChartView()
        addChartElements()
        addNavigationController()
    }
    
    func addNavigationController() {
        guard let navController = navigationController else {
            print("No navigation controller is available. Fix this bug.")
            return
        }
        navController.isToolbarHidden = false
        navController.isNavigationBarHidden = false
        navController.navigationBar.topItem?.title = "Analytics"
        navController.navigationBar.backItem?.title = "Home"
    }
    
    func addChartView() {
        view.addSubview(oChartView)
        oChartView.layer.cornerRadius = 16
        addChartViewConstraints()
    }
    
    func addChartViewConstraints() {
        oChartView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        oChartView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        oChartView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -88).isActive = true
        oChartView.heightAnchor.constraint(equalTo: oChartView.widthAnchor).isActive = true
    }
    
    func addChartElements() {
        view.addSubview(oChartLabelsStackView)
        view.addSubview(oChartBarsStackView)
        addChartLabelsStackViewConstraints()
        addChartBarsStackViewConstraints()
        for element in chart.values {
            
            var label: UILabel {
                let label = UILabel()
                label.text = element.key
                label.textAlignment = .center
                label.translatesAutoresizingMaskIntoConstraints = false
                label.widthAnchor.constraint(equalToConstant: 32).isActive = true
                return label
            }
            oChartLabelsStackView.addArrangedSubview(label)
            
            let barView: UIView = {
                let view = UIView()
                view.backgroundColor = Colors.purple
                view.translatesAutoresizingMaskIntoConstraints = false
                return view
            }()
            oChartBarsStackView.addArrangedSubview(barView)
            
            let barWidth = 32
            let barHeight = 256 * CGFloat(element.value / chart.max)
            barView.widthAnchor.constraint(equalToConstant: CGFloat(barWidth)).isActive = true
            barView.heightAnchor.constraint(equalToConstant: barHeight).isActive = true
            
        }
    }
    
    func addChartLabelsStackViewConstraints() {
        oChartLabelsStackView.topAnchor.constraint(equalTo: oChartView.bottomAnchor).isActive = true
        oChartLabelsStackView.leftAnchor.constraint(equalTo: oChartView.leftAnchor, constant: 16).isActive = true
        oChartLabelsStackView.rightAnchor.constraint(equalTo: oChartView.rightAnchor, constant: -16).isActive = true
        oChartLabelsStackView.heightAnchor.constraint(equalToConstant: 44).isActive = true
    }
    
    func addChartBarsStackViewConstraints() {
        oChartBarsStackView.topAnchor.constraint(equalTo: oChartView.topAnchor).isActive = true
        oChartBarsStackView.leftAnchor.constraint(equalTo: oChartView.leftAnchor, constant: 16).isActive = true
        oChartBarsStackView.rightAnchor.constraint(equalTo: oChartView.rightAnchor, constant: -16).isActive = true
        oChartBarsStackView.bottomAnchor.constraint(equalTo: oChartView.bottomAnchor).isActive = true
    }

}
