//
//  ViewController.swift
//  ChatApp
//
//  Created by Orlando G. Rodriguez on 11/16/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import UIKit

class ConversationViewController: UIViewController {
    
    // MARK: - Model
    //var conversation: Conversation?
    var participantUIDs: [String]?
    var participants: [LocalUser]? {
        didSet {
            updateModel()
//            var filteredMessages: [Message] = []
//            guard let participants = participants else {
//                print("No participants available. Fix this bug.")
//                return
//            }
//            for message in FirebaseService.messages {
//                if (message.value.fromID == participants[0].uid ||
//                    message.value.toID == participants[0].uid) &&
//                   (message.value.fromID == participants[1].uid ||
//                    message.value.toID == participants[1].uid) {
//                    filteredMessages.append(message.value)
//                }
//            }
//            filteredMessages.sort { (msg1, msg2) -> Bool in
//                return msg1.date < msg2.date
//            }
//            messages = filteredMessages
        }
    }
    
    var messages: [Message]? {
        didSet {
            oMessagesTableView.reloadData()
        }
    }
    
    // MARK: - Outlets
    var oMessagesTableView: UITableView = {
        let tableView = UITableView()
        tableView.allowsSelection = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.tableFooterView = UIView()
        return tableView
    }()
    var oNewMessageView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    var oNewMessageTextField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = .white
        textField.layer.cornerRadius = 16
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.borderWidth = 0.5
        textField.translatesAutoresizingMaskIntoConstraints = false
        let padding = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 32))
        textField.leftView = padding
        textField.leftViewMode = .always
        return textField
    }()
    var oSendMessageButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "send"), for: .normal)
        button.addTarget(self, action: #selector(sendButtonPressed), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    // MARK: - Animatable Constraints
    var cNewMessageViewBottomToViewBottom: NSLayoutConstraint?
    var cNewMessageViewBottomToKeyboardTop: NSLayoutConstraint?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateViewFromModel()
    }
    
    @objc func updateModel() {
        var filteredMessages: [Message] = []
        guard let participants = participants else {
            print("No participants available. Fix this bug.")
            return
        }
        for message in FirebaseService.messages {
            if (message.value.fromID == participants[0].uid ||
                message.value.toID == participants[0].uid) &&
                (message.value.fromID == participants[1].uid ||
                    message.value.toID == participants[1].uid) {
                filteredMessages.append(message.value)
            }
        }
        filteredMessages.sort { (msg1, msg2) -> Bool in
            return msg1.date < msg2.date
        }
        messages = filteredMessages
        updateViewFromModel()
    }
    
    @objc
    func updateViewFromModel() {
        print("Updating view from model.")
        guard let navController = navigationController else { return }
        guard let participants = participants else { return }
        navController.navigationBar.topItem?.title = participants[0].email
        navController.navigationBar.backItem?.title = "Messages"
        oMessagesTableView.reloadData()
        let numRows = oMessagesTableView.numberOfRows(inSection: 0)
        if numRows > 0 {
            let indexPath = NSIndexPath(item: numRows - 1, section: 0)
            oMessagesTableView.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
        }
    }
    
    func setupUI() {
        guard let navController = navigationController else {
            print("No navigation controller is available. Please fix this bug.")
            return
        }
        navController.toolbar.isHidden = true
        addKeyboardInputAccessoryView()
        addMessagesTableView()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleHideKeyboard))
        let swipeDown: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleHideKeyboard))
        swipeDown.direction = .down
        view.addGestureRecognizer(tap)
        view.addGestureRecognizer(swipeDown)
    }
    
    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateViewFromModel), name: NSNotification.Name(rawValue: "updatedChatLog"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateModel), name: NSNotification.Name("loadedAllMessages"), object: nil)
    }
    
    // MARK: - UI Element Addition Functions
    func addMessagesTableView() {
        oMessagesTableView.delegate = self
        oMessagesTableView.dataSource = self
        oMessagesTableView.register(MessageCell.self, forCellReuseIdentifier: MessageCell.reuseIdentifier)
        oMessagesTableView.separatorStyle = .none
        view.addSubview(oMessagesTableView)
        addMessagesTableViewConstraints()
    }
    func addKeyboardInputAccessoryView() {
        view.addSubview(oNewMessageView)
        addNewMessageViewConstraints()
        view.addSubview(oSendMessageButton)
        addSendMessageButtonConstraints()
        view.addSubview(oNewMessageTextField)
        addMessageTextFieldConstraints()
    }
    
    // MARK: - Constraint Functions
    func addMessagesTableViewConstraints() {
        oMessagesTableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        oMessagesTableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        oMessagesTableView.bottomAnchor.constraint(equalTo: oNewMessageView.topAnchor).isActive = true
        oMessagesTableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    }
    func addNewMessageViewConstraints() {
        cNewMessageViewBottomToViewBottom = oNewMessageView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -30)
        cNewMessageViewBottomToViewBottom?.isActive = true
        oNewMessageView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        oNewMessageView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        oNewMessageView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        let bottomView = UIView()
        bottomView.backgroundColor = .white
        bottomView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bottomView)
        bottomView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        bottomView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        bottomView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        bottomView.topAnchor.constraint(equalTo: oNewMessageView.bottomAnchor).isActive = true
    }
    func addMessageTextFieldConstraints() {
        oNewMessageTextField.leftAnchor.constraint(equalTo: oNewMessageView.leftAnchor, constant: 32).isActive = true
        oNewMessageTextField.heightAnchor.constraint(equalToConstant: 32).isActive = true
        oNewMessageTextField.centerYAnchor.constraint(equalTo: oNewMessageView.centerYAnchor).isActive = true
        oNewMessageTextField.rightAnchor.constraint(equalTo: oSendMessageButton.leftAnchor).isActive = true
    }
    func addSendMessageButtonConstraints() {
        oSendMessageButton.rightAnchor.constraint(equalTo: oNewMessageView.rightAnchor, constant: -8).isActive = true
        oSendMessageButton.topAnchor.constraint(equalTo: oNewMessageView.topAnchor, constant: 8).isActive = true
        oSendMessageButton.bottomAnchor.constraint(equalTo: oNewMessageView.bottomAnchor, constant: -8).isActive = true
        oSendMessageButton.widthAnchor.constraint(equalToConstant: 64).isActive = true
    }
    
    // MARK: - Handlers
    @objc
    func handleKeyboardWillShow(notification: Notification) {
        let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        UIView.animate(withDuration: 0.3) {
            self.cNewMessageViewBottomToViewBottom?.constant = -keyboardFrame.height
            self.view.layoutIfNeeded()
        }
    }
    @objc
    func handleKeyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.3) {
            self.cNewMessageViewBottomToViewBottom?.constant = -30
            self.view.layoutIfNeeded()
        }
    }
    @objc
    func handleHideKeyboard() {
        view.endEditing(true)
    }
    
    // MARK: - Actions
    @objc
    func sendButtonPressed() {
        guard let text = oNewMessageTextField.text else {
            print("No message available.")
            return
        }
        // print(text)
        guard let participants = participants else { return }
        let toID = participants[0].uid
        let fromID = participants[1].uid
        let message = Message(text: text, toID: toID, fromID: fromID)
        FirebaseService.createNewMessage(message: message)
        oNewMessageTextField.text = ""
    }
    
}

extension ConversationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let messages = messages else { return 0 }
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = MessageCell()
        cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell") as! MessageCell
        cell.setupUI()
        if let messages = messages {
            cell.updateModel(message: messages[indexPath.row])
        } else {
            print("No messages available. Fix this bug.")
        }
        return cell
    }
    
}
