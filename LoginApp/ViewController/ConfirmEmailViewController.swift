//
//  ConfirmEmailViewController.swift
//  LoginApp
//
//  Created by Orlando G. Rodriguez on 11/14/18.
//  Copyright © 2018 orlandogrodriguez. All rights reserved.
//

import UIKit
import FirebaseAuth

class ConfirmEmailViewController: UIViewController {
    
    // MARK: - Models
    let firebaseAPIService = FirebaseAPIService()
    
    // MARK: - Outlets
    var oVerifyLabel: UILabel = {
        let label = UILabel()
        label.text = "Please verify your email address."
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 34)
        label.textAlignment = .center
        label.alpha = 0.0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    var oConfirmedEmailButton: UIButton = {
        let button = UIButton()
        button.setTitle("Already Confirmed", for: .normal)
        button.addTarget(self, action: #selector(confirmedEmailButtonPressed), for: .touchUpInside)
        button.setTitleColor(Colors.red, for: .normal)
        button.backgroundColor = .white
        button.translatesAutoresizingMaskIntoConstraints = false
        button.alpha = 0.0
        return button
    }()
    var oResendConfirmationEmailButton: UIButton = {
        let button = UIButton()
        button.setTitle("Resend confirmation email.", for: .normal)
        button.addTarget(self, action: #selector(resendConfirmationEmailButtonPressed), for: .touchUpInside)
        button.setTitleColor(.white, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.alpha = 0.0
        return button
    }()
    
    // MARK: - Animatable Constraints
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupNotifications()
    }
    
    override func viewDidLayoutSubviews() {
        animateFadeInGreetingLabel()
    }
    
    func setupUI() {
        addGradientBackground()
        addVerifyLabel()
        addConfirmedEmailButton()
        addResendConfirmationEmailButton()
    }
    
    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(animateFadeOutConfirmationEmailUIElements), name: Notification.Name(rawValue: "animateFadeOutConfirmationEmailUIElements"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(displayVerificationEmailSentSuccessfullyAlert), name: Notification.Name(rawValue: "verificationEmailSentSuccessfully"), object: nil)
    }
    
    // MARK: - UI Element Addition Functions
    func addGradientBackground() {
        let layer = CAGradientLayer()
        layer.frame = self.view.bounds
        layer.colors = Colors.intro.gradient()
        self.view.layer.addSublayer(layer)
    }
    func addVerifyLabel() {
        view.addSubview(oVerifyLabel)
        addVerifyLabelConstraints()
    }
    func addConfirmedEmailButton() {
        oConfirmedEmailButton.layer.cornerRadius = 22
        view.addSubview(oConfirmedEmailButton)
        addConfirmedEmailButtonConstraints()
    }
    func addResendConfirmationEmailButton() {
        view.addSubview(oResendConfirmationEmailButton)
        addResendConfirmationEmailButtonConstraints()
    }
    
    // MARK: - Constraint Functions
    func addVerifyLabelConstraints() {
        oVerifyLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        oVerifyLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        oVerifyLabel.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -64).isActive = true
        oVerifyLabel.heightAnchor.constraint(equalToConstant: 88).isActive = true
    }
    func addConfirmedEmailButtonConstraints() {
        oConfirmedEmailButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        oConfirmedEmailButton.topAnchor.constraint(equalTo: oVerifyLabel.bottomAnchor, constant: 16).isActive = true
        oConfirmedEmailButton.widthAnchor.constraint(equalToConstant: 200).isActive = true
        oConfirmedEmailButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
    }
    func addResendConfirmationEmailButtonConstraints() {
        oResendConfirmationEmailButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        oResendConfirmationEmailButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -46).isActive = true
        oResendConfirmationEmailButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -64).isActive = true
        oResendConfirmationEmailButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
    }
    
    // MARK: - Animation Functions
    func animateFadeInGreetingLabel() {
        UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseInOut, animations: {
            self.oVerifyLabel.alpha = 1.0
            self.oConfirmedEmailButton.alpha = 1.0
            self.oResendConfirmationEmailButton.alpha = 1.0
        }) { (true) in
            
        }
    }
    
    // MARK: - Alerts
    @objc func displayVerificationEmailSentSuccessfullyAlert() {
        guard let user = Auth.auth().currentUser else { return }
        let alert = UIAlertController(title: "Verification Email Sent", message: "We have successfully sent a verification email to \(user.email!).", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    func displayVerificationEmailSentUnsuccessfullyAlert() {
        let alert = UIAlertController(title: "Unable To Send Email", message: "We were not able to send a verification email. Check your network connection and try again.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    @objc
    func animateFadeOutConfirmationEmailUIElements() {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.oVerifyLabel.alpha = 0.0
            self.oResendConfirmationEmailButton.alpha = 0.0
            self.oConfirmedEmailButton.alpha = 0.0
        }) { (true) in
            let generator = UIImpactFeedbackGenerator(style: .medium)
            generator.impactOccurred()
            let nextVC: UIViewController = WelcomeViewController()
            self.navigationController?.pushViewController(nextVC, animated: false)
            //self.present(nextVC, animated: false, completion: nil)
        }
    }
    
    // MARK: - Actions
    @objc
    func resendConfirmationEmailButtonPressed() {
        firebaseAPIService.resendConfirmationEmail()
    }
    
    @objc
    func confirmedEmailButtonPressed() {
        guard let user = Auth.auth().currentUser else {
            print("There is no current user logged in.")
            self.dismiss(animated: false, completion: nil)
            return
        }
        user.reload(completion: nil)
        if user.isEmailVerified {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "animateFadeOutConfirmationEmailUIElements"), object: nil)
        } else {
            let alert = UIAlertController(title: "Verify Email Address", message: "You have not yet verified your email address.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true)
        }
    }
}
